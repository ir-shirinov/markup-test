(function () {
  'use strict';


  // Функция запускаемая после загрузки дом дерева
  const onReady = () => {

    const body = document.querySelector('body');
    const headerMobile = document.querySelector('.header-mobile');
    const headerMobileTop = document.querySelector('.header-mobile__top');
    const btnOpenMenuMobile = headerMobile.querySelector('#btn-open-menu');
    const navigation = headerMobile.querySelector('.header-mobile__navigation');
    const mainNav = navigation.querySelector('.header-mobile__nav-main');
    const navigationItemsMenu = navigation.querySelectorAll('.header-mobile__nav-items');
    const btnsNextMainNav = mainNav.querySelectorAll('.header-mobile__next');


    //Если JavaScript включен, то показываем кнопку меню и навигацию вперед, и прячем меню
    btnOpenMenuMobile.classList.remove('header-mobile__hidden');
    navigation.classList.add('header-mobile__hidden', 'header-mobile__navigation-js');
    btnsNextMainNav.forEach(function (it) {
      it.classList.remove('header-mobile__hidden');
    });


    // Функция которая по закрытию меню, ставит все в изначальный вид, чтобы при открытии показывалось главное меню(а не то меню, на котором был закрыт экран)
    const onMakeDefaultMenu = () => {
      // Если второстепенное меню не скрыто, скрываем его
      navigationItemsMenu.forEach((it) => {
        if (it.classList.contains('header-mobile__show')) {
          it.classList.remove('header-mobile__show');
          it.classList.add('header-mobile__hidden');
        };
      });

      // Если главное меню скрыто, то показываем его
      if (mainNav.classList.contains('header-mobile__hidden')) {
        mainNav.classList.remove('header-mobile__hidden')
      };
    };


    // Функция показывает/скрывает меню, меняя значок бургера и запрещая скролл по всей странице, кроме самого меню.
    const onClickOpenСloseMenu = () => {
      btnOpenMenuMobile.classList.toggle('header-mobile__btn-close');
      headerMobile.classList.toggle('header-mobile__scroll');
      headerMobileTop.classList.toggle('header-mobile__top-fixed');
      navigation.classList.toggle('header-mobile__hidden');

      if (navigation.classList.contains('header-mobile__hidden')) {
        body.style.overflow = 'auto';
        onMakeDefaultMenu();
      } else {
        body.style.overflow = 'hidden';
      };
    };


    // Показывает следующее меню
    const showNextMenu = (currentMenu, nextMenu) => {
      const onHiddenCurrentMenu = () => {
        currentMenu.classList.add('header-mobile__hidden');
        nextMenu.removeEventListener('transitionend', onHiddenCurrentMenu);
      }
      nextMenu.addEventListener('transitionend', onHiddenCurrentMenu);
      nextMenu.classList.remove('header-mobile__hidden');
      setTimeout(() => {
        nextMenu.classList.add('header-mobile__show');
      },0);    
    };


    // Показывает предыдущее меню
    const showBackMenu = (currentMenu, prevMenu) => {
      prevMenu.classList.remove('header-mobile__hidden');
      const onHiddenCurrentMenu = () => {
        currentMenu.classList.add('header-mobile__hidden');
        currentMenu.removeEventListener('transitionend', onHiddenCurrentMenu);
      }
      currentMenu.addEventListener('transitionend', onHiddenCurrentMenu);
      setTimeout(() => {
        currentMenu.classList.remove('header-mobile__show');
      },0);
    }


    // Блоки с меню
    const companyNav = navigation.querySelector('.header-mobile__nav-company');
    const aboutNav = navigation.querySelector('.header-mobile__nav-about');
    const teamNav = navigation.querySelector('.header-mobile__nav-team');
    const infocentreNav = navigation.querySelector('.header-mobile__nav-infocentre');
    const contactsNav = navigation.querySelector('.header-mobile__nav-contacts');
    const branchesNav = navigation.querySelector('.header-mobile__nav-branches');
    const galleryNav = navigation.querySelector('.header-mobile__nav-gallery');


    // Кнопки и их соотвествие с меню. На каком меню кнопка, и куда ведет
    const btnAndMenu = {
      company: {
        toMenu: mainNav,
        currentMenu: companyNav,
      },
      about: {
        toMenu: companyNav,
        currentMenu: aboutNav,
      },
      branches: {
        toMenu: companyNav,
        currentMenu: branchesNav,
      },
      contacts: {
        toMenu: mainNav,
        currentMenu: contactsNav,
      },
      gallery: {
        toMenu: companyNav,
        currentMenu: galleryNav,
      },
      team: {
        toMenu: mainNav,
        currentMenu: teamNav,
      },
      infocentre: {
        toMenu: mainNav,
        currentMenu: infocentreNav,
      },
    };


    // Вешаем обработчики событий на все кнопки назад
    const btnsPrevMenu = navigation.querySelectorAll('.header-mobile__back');
    btnsPrevMenu.forEach((it) => {
      const elementName = it.id.split('-')[2];
      it.addEventListener('click', () => {
        showBackMenu(btnAndMenu[elementName].currentMenu, btnAndMenu[elementName].toMenu);
      });
    });


    // Вешаем обработчики событий на все кнопки вперед
    const btnsNextMenu = navigation.querySelectorAll('.header-mobile__next');
    btnsNextMenu.forEach((it) => {
      const elementName = it.id.split('-')[2];
      it.addEventListener('click', () => {
        showNextMenu(btnAndMenu[elementName].toMenu, btnAndMenu[elementName].currentMenu);
      });
    });


    // Вешаем обработчик событий на открытие/закрытие меню
    btnOpenMenuMobile.addEventListener('click', onClickOpenСloseMenu);
  };


  // Запускаем работу модуля, только после загрузки Dom
  document.addEventListener('DOMContentLoaded', onReady);
})();